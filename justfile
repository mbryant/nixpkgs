# Choose between the other options
default:
    @just --choose --justfile {{justfile()}}

# Print the home-manager config directory
dir:
    @echo {{justfile_directory()}}

# Edit home.nix
edit:
    vim {{justfile_directory()}}/home.nix

# Update nixpkgs then do a build
rebuild: && build chmod-desktop-files
    nix flake update

# Build the current nix configuration
build:
    home-manager --impure --flake . switch

# Print info about the current flake
status:
    nix flake info

# Remove stale Nix files
clean:
    nix-collect-garbage --delete-older-than 7d

# Mark .desktop files as executable for KDE
chmod-desktop-files:
    # Rebuild the .desktop files - ^C here if this was a noop build
    @# We know kitty has a desktop file, so we can use it to find the most recent home-manager version
    sudo fd "\.desktop$" "/$(readlink $(which kitty) | choose --field-separator '/' :-3 --output-field-separator '/')" -x chmod +x
