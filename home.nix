{
  config,
  pkgs,
  lib,
  ...
}:

let
  # The directory our home-manager configuration lives in
  home_manager_path = "${config.home.homeDirectory}/.config/home-manager";

  publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHmqpOBlYDOnhyZf9sbLi6BMdpkt50Ul5sgMim5XApiK";

  # Various services need `passage`, which requires environment variables to be
  # set for it to work, and systemd won't use those by default.
  passage_env = with config.programs.password-store.settings; [
    "PASSAGE_DIR=${PASSAGE_DIR}"
    "PASSAGE_IDENTITIES_FILE=${PASSAGE_IDENTITIES_FILE}"
  ];

  # `custom_scripts` is a attrset from { script name -> script } that contains
  # all scripts in `./bin`.
  custom_scripts = builtins.listToAttrs (
    builtins.map
      (
        name:
        lib.attrsets.nameValuePair name
          # Generate a shell script for each script
          (
            pkgs.writeShellApplication {
              name = name;
              text = (builtins.readFile ./bin/${name}.sh);
            }
          )
      )
      # Find all of the script names in `./bin`
      (
        lib.attrsets.mapAttrsToList (filename: _: lib.strings.removeSuffix ".sh" filename) (
          lib.attrsets.filterAttrs (f: type: type == "regular") (builtins.readDir ./bin)
        )
      )
  );

  nixGLWrap = config.lib.nixGL.wrap;
in
{
  ##############################################################################
  # Nix + Home-Manager config
  ##############################################################################
  home.stateVersion = "23.11";
  home.username = "mbryant";
  home.homeDirectory = "/home/mbryant";

  nix = {
    enable = true;

    package = pkgs.nix;

    # We don't care about legacy channels, so do our best to disable them.
    nixPath = [ "" ];
    keepOldNixPath = false;

    settings = {
      experimental-features = [
        "nix-command"
        "flakes"
      ];
    };
  };

  ##############################################################################
  # Installed programs
  ##############################################################################
  home.packages =
    with pkgs;
    [
      nix # Ensure we're managing Nix itself
      nerd-fonts.hack
      ansible
      ast-grep
      cargo-bloat
      cargo-cross
      cargo-flamegraph
      cargo-geiger
      cargo-outdated
      cargo-show-asm
      (nixGLWrap darktable)
      devenv
      discord
      flac2all
      gef # Custom GDB UI
      (nixGLWrap gimp)
      go
      goaccess
      gphoto2
      hotspot # perf visualizer
      huniq
      hyperfine
      jellyfin
      just
      lidarr # Music cataloguing
      lm_sensors # temp/etc monitors
      mold-wrapped # A faster linker
      # moc # TODO: Doesn't work with libasound
      nixgl.auto.nixGLDefault # Exposes `nixGL` to CLI
      noisetorch # Mic noise suppression
      picard # Music tagger
      poop # Hyperfine alternative with HW counters
      protonup-ng
      qbittorrent
      ripgrep
      rsync
      rustup
      signal-desktop
      spacer # Add newlines when stdout pauses
      # steam # TODO: Needs annoying nixGL hacks to work
      tectonic # LaTeX compiler
      tokei
      tree
      (nixGLWrap vlc)
      wget

      ##########################################################################
      # Alternatives
      ##########################################################################
      age # gpg
      choose # cut
      dogdns # dig
      dua # du
      duf # df
      eza # ls
      fd # find
      gitoxide # git
      hexyl # xxd
      lurk # strace
      mtr # traceroute
      procs # ps
      rustscan # nmap
      sd # sed
      uutils-coreutils # general coreutils replacement
      xh # curl

      ##########################################################################
      # Linters/Formatters
      ##########################################################################
      black # Python formatter
      nixfmt-rfc-style
      ruff # Python linter
      nodePackages.prettier # JS linter
    ]
    ++ (
      ##########################################################################
      # Custom scripts
      ##########################################################################
      # We generate a binary for each shell script in ./bin and include it in
      # our path.  Note that these scripts are all checked with `shellcheck`
      # and have some defensive bash options set.
      builtins.attrValues custom_scripts
    );

  ##############################################################################
  # File configuration
  ##############################################################################
  # Configure SSH and commit signing
  home.file.".ssh/id_ed25519.pub".text = publicKey;
  home.file.".ssh/allowed_signers".text = "* ${publicKey}";

  # Configure various tools
  home.file.".cargo/config.toml".text = builtins.readFile ./config/cargo_config.toml;
  home.file.".moc/config".text = builtins.readFile ./config/moc_config;
  home.file.".config/git/gitmessage".text = builtins.readFile ./config/gitmessage;

  # Ensure `.config/shell` always points to our shell so that we can use it via
  # `chsh -s ~/.config/shell`.
  home.file.".config/shell" = {
    text = ''
      #!${lib.getExe pkgs.bash}

      exec ${lib.getExe pkgs.fish}
    '';
    executable = true;
  };

  ##############################################################################
  # Configured programs
  ##############################################################################
  programs = {
    home-manager = {
      enable = true;
    };
    kitty = {
      enable = true;
      # Kitty needs OpenGL acceleration
      package = nixGLWrap pkgs.kitty;

      settings = {
        # UI
        font_family = "Hack";
        font_size = 12;
        tab_bar_style = "powerline";
        tab_powerline_style = "slanted";
        window_padding_width = "0 0 0 2";

        # Window management
        confirm_os_window_close = -2;
        remember_window_size = true;
        scrollback_fill_enlarged_window = true;
        scrollback_lines = 50000;

        # Use the nix-managed shell rather than the OS native one set via `chsh`.
        shell = lib.getExe pkgs.fish;

        # Misc
        enable_audio_bell = false;
        update_check_interval = 0;
      };
      keybindings = {
        "ctrl+shift+t" = "new_tab_with_cwd";
        "shift+right" = "next_tab";
        "shift+left" = "prev_tab";
        "shift+page_up" = "scroll_page_up";
        "shift+page_down" = "scroll_page_down";
        "shift+home" = "scroll_home";
        "shift+end" = "scroll_end";

        # Instead of `Find`, we have to use the scrollback buffer.
        # TODO: There's some way to do this with vim instead of less.
        "ctrl+shift+f" = "show_scrollback";

        # Use Konsole's default shortcut to clear the terminal
        "ctrl+shift+k" = "clear_terminal to_cursor active";
      };
      themeFile = "Molokai";
      extraConfig = ''
        # Open links with ctrl-click rather than just click
        mouse_map left click ungrabbed no_op
        mouse_map ctrl+left release grabbed,ungrabbed mouse_handle_click link
      '';
    };
    bash = {
      enable = true;
    };
    command-not-found = {
      enable = true;
    };
    git = {
      enable = true;
      userName = "Matt Bryant";
      userEmail = "mbryant@programsareproofs.com";

      delta = {
        enable = true;
        options = {
          side-by-side = true;
          diff-so-fancy = true;
          navigate = true;
          hyperlinks = true;
        };
      };

      ignores = [ "*.swp" ];

      lfs = {
        enable = true;
      };

      extraConfig = {
        # Sign all commits using our ssh key
        commit.gpgsign = true;
        gpg.format = "ssh";
        user.signingkey = "~/.ssh/id_ed25519.pub";
        gpg.ssh.allowedSignersFile = "~/.ssh/allowed_signers";

        init.defaultBranch = "main";

        # Sort branches by more recent
        branch.sort = "-committerdate";

        # NOTE: Doesn't really matter because we're using Delta
        diff = {
          algorithm = "histogram";
          colorMoved = "dimmed-zebra";
          mnemonicPrefix = true;
          renames = true;
        };
        merge.conflictstyle = "zdiff3";

        commit = {
          # Display the diff under commit status
          verbose = true;
          template = "${config.home.homeDirectory}/.config/git/gitmessage";
        };

        push = {
          default = "current";
          autoSetupRemote = true;
          # Push local tags to the server
          followTags = true;
        };
        pull.rebase = true;
        fetch = {
          all = true;
          prune = true;
          pruneTags = true;
        };
        submodule.recurse = true;
        rerere = {
          # Record rebase conflicts and try to reapply them when possible
          enabled = true;
          autoupdate = true;
        };
      };
    };
    git-cliff = {
      enable = true;
    };
    ssh = {
      enable = true;

      compression = true;
      serverAliveInterval = 15;
      controlMaster = "auto";
      controlPath = "/tmp/%r@%h:%p";

      extraConfig = ''
        Protocol 2
      '';

      matchBlocks = {
        "mbryant-box" = {
          hostname = "www.programsareproofs.com";
          port = 48086;
        };
        "musicserver" = {
          hostname = "192.168.1.105";
          user = "musicserver";
        };
      };
    };
    fish = {
      enable = true;

      shellAliases = {
        ":q" = "exit";

        # Modern unix tools
        "cat" = "bat";
        "df" = "duf";
        "du" = "dua";
        "ls" = "eza";
        "ps" = "procs";
        "rg" = "rg --smart-case";
        "xxd" = "hexyl";
        "man" = "batman";

        # Coreutils replacements
        "head" = "uutils-head";
        "sort" = "uutils-sort";
        "tail" = "uutils-tail";
        "wc" = "uutils-wc";

        # Tooling for working with this configuration
        "pkgs" = "just --justfile ${home_manager_path}/justfile";
      };

      shellInit = ''
        # Everything should use vi keybindings
        fish_vi_key_bindings

        # Visual mode opens directly in an editor
        bind --mode command v edit_command_buffer

        # Shells should be quiet when started
        set fish_greeting 

        # Terminfo doesn't work with kitty, so pretend we're something else.
        # It's theoretically invalid, but seems to work in practice.
        set TERM xterm-256color
      '';

      functions = {
        edit = {
          description = "Open files matching a pattern";
          body = ''
            set FILENAME $(fd -tf $argv | fzf -0 -1)

            if [ "$FILENAME" != "" ]
              $EDITOR $FILENAME
            end
          '';
        };

        vimrg = {
          description = "Open files containing text matching a pattern";
          body = "$EDITOR $(rg --files-with-matches $argv)";
        };

        # Nix causes issues with `command-not-found`.  This works around that.
        __fish_command_not_found_handler = {
          body = "__fish_default_command_not_found_handler $argv[1]";
          onEvent = "fish_command_not_found";
        };
      };
    };
    vim = {
      enable = true;
      defaultEditor = true;

      settings = {
        background = "dark";
        undodir = [ "~/.config/vim/undo" ];
        undofile = true;

        expandtab = true;
        tabstop = 4;
        shiftwidth = 4;
        smartcase = true;
        ignorecase = true;
        hidden = true;
      };

      extraConfig = ''
        set autoindent  "autoindent on new lines
        set backspace=indent,eol,start  "Better backspacing
        set clipboard=unnamed  "Copy and paste from system clipboard
        set cursorline  "Highlight current line
        set diffopt+=iwhite,algorithm:patience
        set encoding=utf-8 "UTF-8 character encoding
        set equalalways  "Split windows equal size
        set formatoptions=croqjl  "Enable comment line auto formatting and stuff
        set hlsearch  "Highlight on search
        set incsearch  "Start searching immediately
        set laststatus=2
        set lazyredraw  "Don't redraw while running macros (faster)
        set linebreak  "Only wrap on 'good' characters for wrapping
        set listchars=tab:^-,trail:+,eol:$
        set nocompatible  "Kill vi-compatibility
        set ruler  "Show bottom ruler
        set scrolloff=5  "Never scroll off
        set shortmess=aI  "Make the error messages shorter
        set showmatch  "Highlight matching braces
        set softtabstop=4  "Tab spaces in no hard tab mode
        set statusline=%f\ %l\|%v\ %m%=%p%%\ (%Y%R) "Better statusbar
        set t_Co=256 "256 color
        set title  "Set window title to file
        set undolevels=1000  "Keep undo history after a file is closed
        set undoreload=10000 "and reopened
        set wildignore+=*.o,*.swp,*.pyc "Ignore junk files
        set wildmode=longest,list  "Better unix-like tab completion

        " ----- Colorscheme stuff ----- "
        let g:jellybeans_overrides = {
          \'ColorColumn': {'ctermbg': 'Red'},
        \}
        silent colorscheme jellybeans
        filetype plugin indent on
        syntax on

        " ----- Special File Defaults ----- "
        autocmd BufRead,BufNewFile *.c setlocal colorcolumn=80  "Bar at 80 col for C
        autocmd BufWritePost *.tex silent VimtexCompile
        autocmd BufEnter * silent! :lcd%:p:h  "A modern version of autochdir

        " ----- Plugin/Misc Settings ----- "
        let g:matchparen_insert_timeout = 5  " Show matching parens much sooner
        let g:SuperTabDefaultCompletionType = "context"
        let g:vimtex_compiler_method = "tectonic"

        " ---- Airline (statusline) Settings ---- "
        let g:airline_powerline_fonts = 1
        let g:airline_detect_spelllang = 0
        let g:airline_section_b = ""
        let g:airline_section_c = airline#section#create_right(['%f:%l', 'readonly'])
        let g:airline_section_x = "%p%%"
        let g:airline_section_y = "%l/%L☰ %c㏇"
        let g:airline_section_z = airline#section#create_right(['filetype'])
        let g:airline_section_warning = ""
        let g:airline_section_error = ""
        let g:airline_skip_empty_sections = 1
        let g:airline_extensions = []

        " ----- Lint/Fix Settings ---- "
        let g:ale_fix_on_save = 1
        let g:ale_set_highlights = 0
        let g:ale_virtualtext_cursor = 'current'
        let g:ale_sign_error = ""
        let g:ale_sign_warning = ""
        let g:ale_sign_info = ""
        let g:ale_sign_style_error = ''
        let g:ale_sign_style_warning = ''
        " Make ALE's virtualtext be a different color than comments
        highlight ALEVirtualTextError ctermfg=DarkRed
        highlight ALEVirtualTextWarning ctermfg=Red

        let g:ale_fixers = {
        \ '*': ['remove_trailing_lines', 'trim_whitespace'],
        \ 'nix': ['nixfmt'],
        \ 'rust': ['rustfmt'],
        \ 'python': ['black', 'ruff'],
        \}
        let g:ale_linters = {
        \  'javascript': ['prettier'],
        \  'rust': ['analyzer'],
        \  'python': ['black', 'ruff'],
        \}

        " Get rid of help button
        map <F1> <Esc>
        imap <F1> <Esc>

        " Q executes the macro, rather than entering Ex mode
        nnoremap Q @q

        " Get rid of warning on common capitalization typos
        command WQ wq
        command Wq wq
        command W w
        command Q q
        command E e
        command Vsp vsp
        command Sp sp

        " :w!! does a sudo write
        cnoreabbrev w!! w !sudo tee % >/dev/null

        function! s:find_git_root()
          return system('git rev-parse --show-toplevel 2> /dev/null')[:-2]
        endfunction
        command! ProjectFiles execute 'Files' s:find_git_root()

        " Map <Ctrl-f> to rg for the word under the current cursor
        " TODO: We want <Ctrl-F> to search in the "project directory"
        nnoremap <C-f> :execute "Rg " . expand("<cword>")<CR>

        " Map <Ctrl-Space> to open the file viewer for the current project root
        nnoremap <C-@> :ProjectFiles<CR>
      '';

      plugins = with pkgs.vimPlugins; [
        vim-airline
        vim-airline-themes
        ale
        fzf-vim
        jellybeans-vim
        rust-vim
        supertab
        vim-nix
        vimtex
      ];
    };
    htop = {
      enable = true;

      settings =
        {
          delay = 15;
          detailed_cpu_time = 0;
          enable_mouse = 1;
          find_comm_in_cmdline = 1;
          header_margin = 0;
          highlight_base_name = 1;
          highlight_changes = 1;
          highlight_megabytes = 1;
          highlight_threads = 1;
          screen_tabs = 1;
          shadow_other_users = 1;
          show_cpu_frequency = 1;
          show_cpu_usage = 1;
          show_program_path = 1;
          strip_exe_from_cmdline = 1;
          fields = with config.lib.htop.fields; [
            PID
            USER
            PRIORITY
            NICE
            M_SIZE
            M_RESIDENT
            PERCENT_CPU
            PERCENT_MEM
            TIME
            COMM
          ];
        }
        // (
          with config.lib.htop;
          leftMeters [
            (bar "LeftCPUs2")
            (bar "Memory")
            (text "NetworkIO")
            (text "Uptime")
          ]
          // rightMeters [
            (bar "RightCPUs2")
            (text "Tasks")
            (text "LoadAverage")
            (text "Systemd")
          ]
        );
    };
    bat = {
      enable = true;

      config = {
        theme = "Monokai Extended";
        # In particular, we don't want anything in the left column because it
        # makes copying annoying.
        style = "header,grid,snip";
      };

      extraPackages = with pkgs.bat-extras; [ batman ];
    };
    readline = {
      enable = true;

      variables = {
        editing-mode = "vi";
      };
    };
    thunderbird = {
      enable = true;

      profiles = {
        "default" = {
          isDefault = true;
        };
      };

      settings = {
        "mail.spellcheck.inline" = true;
        "mail.SpellCheckBeforeSend" = true;
        "mail.display_glyph" = false;
        "mail.compose.big_attachments.notify" = false;
        "mailnews.message_display.disable_remote_image" = true;
        "mailnews.mark_message_read.delay" = true;
        "mailnews.mark_message_read.delay.interval" = 1;
      };
    };
    starship = {
      enable = true;

      settings = {
        format = lib.concatStrings [
          "$username$hostname$directory"
          "$git_branch$shlvl$cmd_duration$fill$time"
          "$line_break$jobs$status$character"
        ];
        command_timeout = 50;
        add_newline = false;

        cmd_duration = {
          format = "[⏱ $duration]($style)";
          min_time = 1000;
          show_milliseconds = true;
        };
        directory = {
          truncation_length = 0;
          truncate_to_repo = false;
          truncation_symbol = ".../";
        };
        fill = {
          symbol = " ";
        };
        git_branch = {
          format = "[$symbol$branch]($style) ";
        };
        username = {
          format = "[$user]($style)";
        };
        hostname = {
          format = "[@$hostname$ssh_symbol]($style)";
        };
        status = {
          disabled = false;
        };
        shlvl = {
          disabled = false;
          format = "[$symbol$shlvl]($style)";
          symbol = "🐚 ";
          style = "bold dimmed yellow";
        };
        time = {
          format = "[$time]($style)";
          disabled = false;
        };
      };
    };
    fzf = {
      enable = true;

      defaultOptions = [ "--preview='bat {}'" ];

      # Jellybeans colorscheme
      colors = {
        fg = "188";
        bg = "233";
        hl = "103";
        "fg+" = "222";
        "bg+" = "234";
        "hl+" = "104";
        info = "183";
        prompt = "110";
        spinner = "107";
        pointer = "167";
        marker = "215";
      };
    };
    jq = {
      enable = true;
    };
    tealdeer = {
      enable = true;

      settings = {
        display = {
          compact = true;
        };
        updates = {
          auto_update = true;
        };
      };
    };
    borgmatic = {
      enable = true;

      backups = {
        qbittorrent = {
          # Backup .torrent files so we don't lose info
          location = {
            sourceDirectories = [ "${config.home.homeDirectory}/.local/share/qBittorrent/BT_backup" ];
            repositories = [
              {
                path = "ssh://w0jt2rck@w0jt2rck.repo.borgbase.com/./repo";
                label = "borgbase";
              }
            ];
          };

          storage = {
            encryptionPasscommand = "${lib.getExe pkgs.passage} qbittorrent-backup";

            extraConfig = {
              compression = "auto,zstd";
            };
          };

          retention = {
            # Keep recent backups and some older ones just in case of failures.
            keepMonthly = 3;
            keepWithin = "1w";
          };
        };
        documents = {
          # TODO: Back up shell history also
          location = {
            sourceDirectories = [ "${config.home.homeDirectory}/Documents" ];
            repositories = [
              {
                path = "ssh://x974mh99@x974mh99.repo.borgbase.com/./repo";
                label = "borgbase";
              }
            ];

            extraConfig = {
              exclude_patterns = [
                "*/.git/objects/"
                "*/.git/modules/"
                "*.swp"
              ];
            };
          };

          storage = {
            encryptionPasscommand = "${lib.getExe pkgs.passage} documents-backup";

            extraConfig = {
              compression = "auto,zstd";
            };
          };

          retention = {
            # Keep everything in here for 2y, since things might be needed
            # during the next tax season.
            keepWithin = "2y";
          };
        };
      };
    };
    password-store = {
      # Manages various passwords that Nix needs
      enable = true;
      package = pkgs.passage;

      settings = {
        "PASSAGE_DIR" = "${home_manager_path}/nix-passwords";
        "PASSAGE_IDENTITIES_FILE" = "${home_manager_path}/nix-passwords/passage-identity";
      };
    };
    himalaya = {
      # CLI email client
      enable = true;
    };
    firefox = {
      enable = true;
      package = nixGLWrap pkgs.firefox;
      profiles.default = {
        settings = {
          "browser.aboutConfig.showWarning" = false;
          # UI
          "browser.tabs.inTitlebar" = true;
          "browser.toolbars.bookmarks.visibility" = "never";
          # No sponsored anything
          "browser.discovery.enabled" = false;
          "browser.urlbar.showSearchSuggestionsFirst" = false;
          "browser.urlbar.suggest.quicksuggest.sponsored" = false;
          "browser.urlbar.sponsoredTopSites" = false;
          "browser.newtabpage.activity-stream.showSponsoredTopSites" = false;
          # Devtools config
          "devtools.everOpened" = true;
          "devtools.theme" = "light";
          # Autofilling is annoying and not often useful
          "extensions.formautofill.addresses.enabled" = false;
          "extensions.formautofill.creditCards.enabled" = false;
          # Security/privacy settings
          "network.trr.mode" = 3; # DNS-over-HTTPS
          "dom.security.https_only_mode" = true;
          "privacy.trackingprotection.enabled" = true;
          # Initial Firefox Sync config - login is needed after this
          "services.sync.username" = "firefox@programsareproofs.com";
        };
        search = {
          force = true;
          default = "Google";
        };
      };
    };
    pay-respects = {
      # `f` to auto-correct previous CLI mistakes
      enable = true;
    };
  };

  ##############################################################################
  # General desktop configuration
  ##############################################################################
  targets.genericLinux.enable = true;
  xdg.enable = true;
  xdg.mime.enable = true;

  fonts.fontconfig.enable = true;
  nixGL = {
    packages = pkgs.nixgl.auto;
    defaultWrapper = "nvidia";
    vulkan.enable = true;
  };

  # Desktop entries for apps that don't come with one.
  #
  # NOTE: On KDE we need to `chmod +x` these files for them to work.
  # NOTE: `pkgs build` should handle this properly.
  xdg.desktopEntries = {
    kitty = with pkgs; {
      name = "Kitty";
      icon = "utilities-terminal";
      exec = "kitty";
      comment = "Kitty terminal emulator";
    };
  };

  # Various desktop entries don't use full paths for some reason, so we need to
  # tell KDE to use our path.
  home.file.".config/plasma-workspace/env/path.sh".text = ''
    export PATH=$PATH:$HOME/.nix-profile/bin
  '';

  ##############################################################################
  # Accounts
  ##############################################################################
  accounts.email.accounts = {
    mbryant = {
      primary = true;
      address = "mbryant@programsareproofs.com";
      userName = "mbryant@programsareproofs.com";
      realName = "Matt Bryant";
      signature = {
        text = "- Matt";
        delimiter = "";
        showSignature = "append";
      };
      passwordCommand = "${lib.getExe pkgs.passage} email-password";
      imap = {
        host = "imap.zoho.com";
        port = 993;
        tls.enable = true;
      };
      smtp = {
        host = "smtp.zoho.com";
        port = 465;
        tls.enable = true;
      };
      thunderbird.enable = true;
      himalaya.enable = true;
    };
  };

  ##############################################################################
  # Configured services/timers
  ##############################################################################
  services = {
    borgmatic = {
      enable = true;
      frequency = "daily";
    };
    ollama = {
      # Run local LLMs
      enable = true;

      acceleration = "cuda";
    };
  };

  systemd.user.services.borgmatic.Service.Environment = passage_env;

  ##############################################################################
  # Custom services
  ##############################################################################
  systemd.user.services = {
    valheim = {
      Unit = {
        Description = "Run my valheim socket-activation wrapper";
      };

      Install = {
        WantedBy = [ "default.target" ];
      };

      Service = {
        ExecStart = "${config.home.homeDirectory}/valheim_udp/target/release/valheim_udp";
      };
    };

    dirtgrabber = {
      Unit = {
        Description = "Dirtgrabber Collaborative Server";
      };

      Install = {
        WantedBy = [ "default.target" ];
      };

      Service = {
        ExecStart = "${config.home.homeDirectory}/dirtgrabber/target/release/dirtgrabber-server";
        WorkingDirectory = "${config.home.homeDirectory}/dirtgrabber";
        Restart = "on-failure";
      };
    };

    dns_update = {
      Unit = {
        Description = "Update dns for *.programsareproofs.com";
      };

      Service = {
        ExecStart = lib.getExe custom_scripts.dns_update;
        Environment = passage_env;
        Restart = "on-failure";
      };
    };

    jellyfin = {
      Unit = {
        Description = "Run jellyfin as a network-local movie server";
      };

      Install = {
        WantedBy = [ "default.target" ];
      };

      Service = {
        ExecStart = lib.getExe pkgs.jellyfin;
        Restart = "on-failure";
      };
    };

    photos_backend = {
      Unit = {
        Description = "Run the backend for photos.programsareproofs.com";
      };

      Install = {
        WantedBy = [ "default.target" ];
      };

      Service = {
        Environment = "PHOTO_DIR=${config.home.homeDirectory}/Pictures";
        ExecStart = "${config.home.homeDirectory}/public_html/Photos/backend/target/release/photo_server";
        # We do graceful exits sometimes when we want to restart, and there's
        # no need to wait for doing so.
        Restart = "always";
        RestartSec = "0";
      };
    };

    sending_quest = {
      Unit = {
        Description = "https://sending.quest";
      };

      Install = {
        WantedBy = [ "default.target" ];
      };

      Service = {
        # Configure `passage` so we can read out secrets.
        Environment = passage_env;
        ExecStart = "${lib.getExe pkgs.bash} -c 'env SENDGRID_KEY=$(${lib.getExe pkgs.passage} sendingquest-sendgrid) ROCKET_secret_key=$(${lib.getExe pkgs.passage} sendingquest-rocket-key) ${config.home.homeDirectory}/sending.quest/target/release/sendingquest'";
        WorkingDirectory = "${config.home.homeDirectory}/sending.quest";
        Restart = "on-failure";
      };
    };
  };

  systemd.user.timers = {
    dns_update = {
      Unit = {
        Description = "Continuously update DNS for *.programsareproofs.com";
      };

      Timer = {
        Unit = "dns_update.service";
        OnCalendar = "*:0/3";
      };

      Install = {
        WantedBy = [ "timers.target" ];
      };
    };
  };

  # Ensure KDE has an accurate view of our .desktop files
  home.activation.updateKdeIconCache = lib.hm.dag.entryAfter [ "installPackages" ] ''
    run /usr/bin/kbuildsycoca5 --noincremental
  '';
}
