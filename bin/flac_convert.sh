set -e

INPUT=$(realpath "$1")

PATH_320=$(sd FLAC 320 <<< "$INPUT")
PATH_V0=$(sd FLAC V0 <<< "$INPUT")

# The Python script is currently broken when showing the summary.  This doesn't
# impact anything, so ignore failures.
# Gross, but seems to work.
flac2all mp3 --copy --nodirs --lame-options='b 320:h' -o "$PATH_320" "$INPUT" > /dev/null || true
flac2all mp3 --copy --nodirs --lame-options='V 0' -o "$PATH_V0" "$INPUT" > /dev/null || true

for output in "$PATH_320" "$PATH_V0"; do
    # Remove the garbage 'mp3' intermediate directory
    cd "$output"
    mv mp3/* .
    rmdir mp3
done

for dir in "$INPUT" "$PATH_320" "$PATH_V0"; do
    # Print the new directory + contents
    echo "$dir"
    eza --color=always -l "$dir"
done
