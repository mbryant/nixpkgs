# A script to keep `programsareproofs.com`'s DNS up-to-date when on a dynamic
# IP
set -e

ZONE_ID="24346744a77f1fd8b3e4f132f7181a99"
UPDATE_URL="https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records"
API_KEY=$(passage cloudflare-api-key)
API_EMAIL=$(passage cloudflare-api-email)

# Fetched via:
# curl -X GET "$UPDATE_URL?type=A&name=programsareproofs.com" \
#      -H "X-Auth-Email: $API_EMAIL" \
#      -H "X-Auth-Key: $API_KEY" \
#      -H "Content-Type: application/json"
IPV4_RECORD_ID="9ff1540f3e66576c65919b507992abac"
# Fetched via:
# curl -X GET "$UPDATE_URL?type=AAAA&name=programsareproofs.com" \
#      -H "X-Auth-Email: $API_EMAIL" \
#      -H "X-Auth-Key: $API_KEY" \
#      -H "Content-Type: application/json"
IPV6_RECORD_ID="1e3be82b4957b17af5fea45e8bdb5a36"

NEW_IP1=$(wget -qO- https://ipinfo.io/ip)
NEW_IP2=$(wget -qO- https://ipv4.icanhazip.com)

if [ "$NEW_IP1" != "$NEW_IP2" ]; then
    echo "External ip source compromised? $NEW_IP1 vs $NEW_IP2"
    exit 127
else
    IPV4="$NEW_IP1"
fi

IPV6=$(ip -6 addr | rg global | xargs | choose 1 | choose -f '/' 0)

# Update cloudflare's dns with our current values
echo "IPv4 => $IPV4"
xh --ignore-stdin PUT "$UPDATE_URL/$IPV4_RECORD_ID" \
     "X-Auth-Email: $API_EMAIL" \
     "X-Auth-Key: $API_KEY" \
     "Content-Type: application/json" \
     "name=programsareproofs.com" "type=A" "content=$IPV4" "ttl:=1" "proxied:=false" | jq .success

echo "IPv6 => $IPV6"
xh --ignore-stdin PUT "$UPDATE_URL/$IPV6_RECORD_ID" \
     "X-Auth-Email: $API_EMAIL" \
     "X-Auth-Key: $API_KEY" \
     "Content-Type: application/json" \
     "name=programsareproofs.com" "type=AAAA" "content=$IPV6" "ttl:=1" "proxied:=false" | jq .success
