# Backs up directories that are too large to cheaply be backed up remotely

if [ $# -eq 0 ]; then
    echo "Usage: local_backup [BACKUP_TARGET]"
    exit 1
fi

TARGET_DIR=$1
if [ ! -d "${TARGET_DIR}" ]; then
    echo "BACKUP_TARGET \`${TARGET_DIR}\` doesn't exist"
    exit 1
fi

# Create a temporary config file for borgmatic to use
CONFIG=$(cat <<HEREDOC
source_directories:
    - /mnt/8tb
    - ~/Pictures
    - ~/Documents
repositories:
    - path: $TARGET_DIR
      label: local
exclude_patterns:
    # Movies are too big to reasonably backup, and I can always re-rip
    # them.
    - '*Movies/'
encryption_passcommand: passage local-backup
compression: auto,zstd
keep_monthly: 12
HEREDOC
)
CONFIG_FILE=$(mktemp /tmp/borgmatic.local_backup.XXXXXX.yaml)
echo "${CONFIG}" > "$CONFIG_FILE"

# Actually do the backup
borgmatic --config "${CONFIG_FILE}" create --list --stats

# TODO: Validate the backup?
echo "Run \`borgmatic --config ${CONFIG_FILE}\` to work with the backup repository."
