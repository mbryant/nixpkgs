# A simple terminal `git log` pager, based on
# https://github.com/junegunn/fzf/wiki/Examples#git

# We take this complicated approach since we want to override delta's
# configuration to not show side-by-side view since we don't have the
# horizontal screen real estate for that.
_viewGitLogLine="echo {} | choose -f ' ' 0 | xargs -I % sh -c 'git show --color=always %' | delta --config /dev/null --syntax-theme 'Monokai Extended' --diff-so-fancy"

exec git log --no-merges --color=always --format="%C(auto)%h %s %C(black)%C(bold)%cr% C(auto)%an" "$@" |
    fzf --no-sort --reverse --tiebreak=index --no-multi \
        --ansi --preview="$_viewGitLogLine" \
        --header "enter to view" \
        --bind "enter:execute:$_viewGitLogLine   | less -R" \
