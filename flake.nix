{
  description = "Home-manager";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixGL = {
      url = "github:nix-community/nixGL";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    devenv.url = "github:cachix/devenv/latest";
  };

  outputs =
    {
      nixpkgs,
      home-manager,
      nixGL,
      devenv,
      ...
    }:
    {
      defaultPackage.x86_64-linux = home-manager.defaultPackage.x86_64-linux;

      homeConfigurations.mbryant = home-manager.lib.homeManagerConfiguration {

        pkgs = import nixpkgs {
          system = "x86_64-linux";

          # We want to install various non-OSS packages
          config.allowUnfree = true;

          overlays = [
            # We need nixGL to workaround nvidia issues
            nixGL.overlay

            # Devenv isn't in `nixpkgs` for some reason
            (self: super: { devenv = devenv.packages.x86_64-linux.devenv; })
          ];
        };
        modules = [ ./home.nix ];
      };
    };
}
