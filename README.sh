#!/bin/bash
set -e

# Clone this repo
cd ~/.config
git clone https://gitlab.com/mbryant/home-configuration.git home-manager
cd home-manager

# Install `nix`
sh <(curl -L https://nixos.org/nix/install) --daemon

# Build and enable `home-manager` for the first time
nix build --impure --no-link '.#homeConfigurations.mbryant.activationPackage'
$(nix path-info --impure '.#homeConfigurations.mbryant.activationPackage')/activate

# Setup `passage` to decrypt Nix secrets
age -d passage-identity.age > nix-passwords/passage-identity

# Install our SSH private key (public is managed via Nix)
passage ssh-private-key > ~/.ssh/id_ed25519
chmod 600 ~/.ssh/id_ed25519

# Switch the repo to use SSH again
git remote set-url origin git@gitlab.com:mbryant/home-configuration.git
